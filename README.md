
# Junit-demo APP

Spring boot application which is created to show the unit testing in practise
The application serves the api to storing/retrieving the customers
Application store the data in h2 database
There are two integration tests used to test database connection and rest controller itself

## Specification

* language: java
* version: oraclejdk8
* open in ide: the lombok plugin is required (https://stackoverflow.com/questions/41161076/adding-lombok-plugin-to-intellij-project
### Running the tests
The maven is used as a building tool (if You don't have the maven installed the maven wrapper is also shipped)

The application has to profiles:
* default (fast) - executes only the unit tests 
```
mvn clean install
```
or using mvn wrapper
```
./mvnw clean install
```
* itest (slow) - exeutes all tests (unit + integration)

```
mvn clean install -Pitest
```
or using mvn wrapper
```
./mvnw clean install -Pitest
```
#### Tests configuration
* The surefire plugin is used to execute unit tests
* The failsafe is used to executed the integration tests (ends with IntegrationTest)

look at description
https://stackoverflow.com/questions/28986005/what-is-the-difference-between-the-maven-surefire-and-maven-failsafe-plugins

### Deployment
The spring boot is used to start the application on tomcat server
```
mvn spring-boot:run
```
After running the online swagger documentation is available http://localhost:8080/swagger-ui.html
### Additional info

