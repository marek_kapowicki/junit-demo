package pl.marekk.junitdemo.domain.customer;

import org.assertj.core.util.Lists;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static pl.marekk.junitdemo.domain.customer.CustomerEntityAssembler.sample;

public class S04_CollectionAssertionTest {
    @Test
    public void shouldAssertCollectionEmails() {
        //Given
        List<CustomerEntity> clubs = Lists.newArrayList(sample().email("manu@england.com").assemble(),
                sample().email("fcbarca@spain.com").assemble());
        SuperAdvanceLogic logic = SuperAdvanceLogic.of(clubs);
        //When
        List<CustomerSnapshot> result = logic.apply();
        //Then
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals("manu@england.com", result.get(0).getEmail());
        assertEquals("fcbarca@spain.com", result.get(1).getEmail());
    }

    @Test
    public void shouldAssertEmailsInOneAssertion() {
        //Given
        List<CustomerEntity> clubs = Lists.newArrayList(sample().email("manu@england.com").assemble(),
                sample().email("fcbarca@spain.com").assemble());
        SuperAdvanceLogic logic = SuperAdvanceLogic.of(clubs);
        //When
        List<CustomerSnapshot> result = logic.apply();
        //Then
        assertThat(result)
                .isNotNull()
                .extracting(CustomerSnapshot::getEmail)
                .containsExactly("manu@england.com", "fcbarca@spain.com");
    }

    @Test
    public void shouldAssertResultInOneAssertion() {
        //Given
        List<CustomerEntity> clubs = Lists.newArrayList(
                sample()
                        .firstName("man")
                        .lastName("und")
                        .email("manu@england.com").assemble(),
                sample()
                        .firstName("fc")
                        .lastName("barcelona")
                        .email("fcbarca@spain.com").assemble());
        SuperAdvanceLogic logic = SuperAdvanceLogic.of(clubs);
        //When
        List<CustomerSnapshot> result = logic.apply();
        //Then
        assertThat(result)
                .isNotNull()
                .extracting("fullName", "email")
                .contains(
                        tuple("fc barcelona","fcbarca@spain.com"),
                        tuple("man und", "manu@england.com"));
    }
}
