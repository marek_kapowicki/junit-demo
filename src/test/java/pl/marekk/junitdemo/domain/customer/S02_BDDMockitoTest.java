package pl.marekk.junitdemo.domain.customer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.marekk.junitdemo.domain.customer.CustomerExamples.MAREK_ENTITY;

@RunWith(MockitoJUnitRunner.class)
public class S02_BDDMockitoTest {

    @Mock
    private CustomerRepository customerRepository;
    @InjectMocks
    private CustomerFacade customerFacade;

    @Test
    public void shouldMockDataRepisitory() {
        //Given
        String id = "id";
        when(customerRepository.findByUuid(id)).thenReturn(Optional.of(MAREK_ENTITY));
        //When
        CustomerSnapshot found = customerFacade.retrieve(id);
        //Then
        verify(customerRepository, times(1)).findByUuid(id);
        assertEquals(found.getFullName(), "marek kapowicki");
    }

    @Test
    public void shouldMockDataRepisitoryInMoreBDDWay() {
        //Given
        String id = "id";
        given(customerRepository.findByUuid(id)).willReturn(Optional.of(MAREK_ENTITY));
        //When
        CustomerSnapshot found = customerFacade.retrieve(id);
        //Then
        verify(customerRepository).findByUuid(id);
        assertEquals(found.getFullName(), "marek kapowicki");
    }
}