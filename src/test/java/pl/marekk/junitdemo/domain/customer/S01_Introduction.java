package pl.marekk.junitdemo.domain.customer;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static pl.marekk.junitdemo.domain.customer.CustomerEntityAssembler.sample;

public class S01_Introduction {
    @Test
    public void shouldGenerateCustomerSnapshot() {
        //Given
        CustomerEntity snow = sample()
                .firstName("Jon")
                .lastName("Snow").assemble();
        //When
        CustomerSnapshot snowSnapshot = snow.toSnapshot();
        //Then
        assertEquals("Jon Snow", snowSnapshot.getFullName()); //dont remember which is actual and which expected
    }

    @Test
    public void shouldGenerateCustomerSnapshot_fluentAssertion() {
        //Given
        CustomerEntity snow = sample()
                .firstName("Jon")
                .lastName("Snow").assemble();
        //When
        CustomerSnapshot snowSnapshot = snow.toSnapshot();
        //Then
        assertThat(snowSnapshot.getFullName())
                .isNotEqualTo("marek")
                .isEqualToIgnoringCase("jon snow");
    }
}
