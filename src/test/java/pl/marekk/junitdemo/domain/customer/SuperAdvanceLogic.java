package pl.marekk.junitdemo.domain.customer;

import lombok.AllArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor(staticName = "of")
public class SuperAdvanceLogic {
    private List<CustomerEntity> entities;

    List<CustomerSnapshot> apply() {
        return entities.stream()
                .map(CustomerEntity::toSnapshot)
                .collect(Collectors.toList());
    }
}
