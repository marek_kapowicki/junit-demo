package pl.marekk.junitdemo.domain.customer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.marekk.junitdemo.domain.customer.CustomerExamples.MAREK_ENTITY;

@DataJpaTest
@ContextConfiguration(classes = CustomerConfiguration.class)
@RunWith(SpringRunner.class)
public class CustomerRepositoryIntegrationTest {
    @Autowired
    private CustomerRepository customerRepository;
    private String storedId;

    @Before
    public void setUp() {
        storedId = customerRepository.save(MAREK_ENTITY).getUuid();
    }

    @Test
    public void shouldRetrieveThePrestoredEntity() {
        //When
        Optional<CustomerEntity> found = customerRepository.findByUuid(storedId);
        //Then
        assertThat(found).isPresent()
                .hasValue(MAREK_ENTITY);
    }
}