package pl.marekk.junitdemo.domain.customer;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomerEntityAssembler {
    private String firstName;
    private String lastName;
    private String email;

    public static CustomerEntityAssembler sample() {
        return new CustomerEntityAssembler();
    }

    public CustomerEntityAssembler firstName(String firstName){
        this.firstName = firstName;
        return this;
    }

    public CustomerEntityAssembler lastName(String lastName){
        this.lastName = lastName;
        return this;
    }

    public CustomerEntityAssembler email(String email){
        this.email = email;
        return this;
    }

    public CustomerEntity assemble() {
        return new CustomerEntity(firstName, lastName, email);
    }

}