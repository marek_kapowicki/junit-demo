package pl.marekk.junitdemo.domain.customer.api;

import com.jayway.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import pl.marekk.junitdemo.domain.customer.CustomerFacade;
import pl.marekk.junitdemo.domain.customer.CustomerRegisterCommand;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Matchers.any;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static pl.marekk.junitdemo.api.Specification.API_CONTENT_TYPE;
import static pl.marekk.junitdemo.api.Specification.ROOT;
import static pl.marekk.junitdemo.domain.customer.CustomerExamples.MAREK_SNAPSHOT;
import static pl.marekk.junitdemo.domain.customer.api.RequestExamples.toJson;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@RunWith(SpringRunner.class)
public class CustomerControllerIntegrationTest {

    @MockBean
    private CustomerFacade customerFacade;

    @Value("${local.server.port}")
    private int serverPort;

    @Before
    public void setUp() {
        RestAssured.port = serverPort;
    }

    @Test
    public void shouldCreateCustomer() {
        //Given
        BDDMockito.given(customerFacade.register(any(CustomerRegisterCommand.class))).willReturn("newId");
        //Expect
        given()
            .contentType(API_CONTENT_TYPE)
            .body(toJson(RequestExamples.CUSTOMER_CORRECT_REQUEST))
        .when()
            .post(ROOT + "/customers")
        .then()
            .statusCode(HttpStatus.CREATED.value())
            .body("id", equalTo("newId"));
    }

    @Test
    public void shouldReturnTheCreatedCustomer() {
        //Given
        String existingId = "id1";
        BDDMockito.given(customerFacade.retrieve(existingId)).willReturn(MAREK_SNAPSHOT);
        //Expect
        given()
            .pathParam("customerId", existingId)
        .when()
            .get(ROOT + "/customers/{customerId}")
        .then()
            .statusCode(org.apache.http.HttpStatus.SC_OK)
            .contentType(API_CONTENT_TYPE)
            .body("identityNo", notNullValue()).and()
            .body("fullName", notNullValue()).and()
            .body("email", notNullValue());
    }
}