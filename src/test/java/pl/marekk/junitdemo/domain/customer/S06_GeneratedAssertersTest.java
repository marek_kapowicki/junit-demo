package pl.marekk.junitdemo.domain.customer;

import org.junit.Test;

import static pl.marekk.junitdemo.domain.customer.CustomerExamples.MAREK_ENTITY;

//mvn assertj:generate-assertions
public class S06_GeneratedAssertersTest {
    @Test
    public void shouldUseGeneratedAsserter() {
        //When
        CustomerSnapshot snapshot = MAREK_ENTITY.toSnapshot();
        //Then
//        CustomerSnapshotAssert.assertThat(snapshot)
//                .hasFullName("marek kapowicki")
//                .hasEmail("mk@gmail.com");
    }
}
