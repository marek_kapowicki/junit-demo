package pl.marekk.junitdemo.domain.customer;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.context.config.ResourceNotFoundException;
import pl.marekk.junitdemo.domain.exception.ResourceNotFound;

import java.util.Optional;

import static org.assertj.core.api.Java6Assertions.assertThatCode;
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static pl.marekk.junitdemo.domain.customer.CustomerExamples.MAREK_ENTITY;

@RunWith(MockitoJUnitRunner.class)
public class S03_ExceptionTest {
    @Mock
    private CustomerRepository customerRepository;
    @InjectMocks
    private CustomerFacade customerFacade;

    @Rule
    public ExpectedException thrownException = ExpectedException.none();


    @Test(expected = ResourceNotFound.class)
    public void breakTheBDDConvention() {
        //Given
        given(customerRepository.findByUuid(any())).willReturn(Optional.empty());
        //When
        customerFacade.retrieve("someId");
        //Then expect exception
    }

    @Test
    public void useTheAssertJ() {
        //Given
        given(customerRepository.findByUuid(any())).willReturn(Optional.empty());
        //Expect
        assertThatThrownBy(() -> customerFacade.retrieve("someId"))
                .isInstanceOf(ResourceNotFound.class)
                .hasMessageContaining("not found");
    }

    @Test
    public void shouldVerifyNotThrown() {
        //Given
        String id = "id";
        given(customerRepository.findByUuid(id)).willReturn(Optional.of(MAREK_ENTITY));
        //Expect
        assertThatCode(() -> customerFacade.retrieve(id))
                .doesNotThrowAnyException();
    }
}
