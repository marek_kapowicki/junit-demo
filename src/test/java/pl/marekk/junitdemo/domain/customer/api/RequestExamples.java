package pl.marekk.junitdemo.domain.customer.api;

import groovy.json.JsonBuilder;
import io.codearte.jfairy.Fairy;
import io.codearte.jfairy.producer.person.Person;
import pl.marekk.junitdemo.domain.customer.CustomerRegisterCommand;

public class RequestExamples {

    static final CustomerRegisterRequest CUSTOMER_CORRECT_REQUEST = generate();

    static String toJson(CustomerRegisterRequest request) {
        CustomerRegisterCommand customer = request.toCommand();
        return new JsonBuilder(customer).toPrettyString();
    }
    static CustomerRegisterRequest generate() {
        Person person = Fairy.create().person();
        return new CustomerRegisterRequest(person.getFirstName(), person.getLastName(), person.getEmail());
    }
}
