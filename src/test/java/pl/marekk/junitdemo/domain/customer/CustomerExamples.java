package pl.marekk.junitdemo.domain.customer;

import pl.marekk.junitdemo.domain.customer.CustomerSnapshot;

public class CustomerExamples {
    public static final CustomerSnapshot MAREK_SNAPSHOT = new CustomerSnapshot("id1", "marek k", "mk@gmail.com");
    public static final CustomerEntity MAREK_ENTITY = new CustomerEntity("marek", "kapowicki", "mk@gmail.com");
}
