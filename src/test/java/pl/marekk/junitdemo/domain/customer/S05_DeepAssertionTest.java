package pl.marekk.junitdemo.domain.customer;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class S05_DeepAssertionTest {

    @Test
    public void standardEqualDoesNotWork() {
        //Given
        SomeObject o1 = new SomeObject("name", 1);
        SomeObject o2 = new SomeObject("name", 1);
        //Expect
        assertThat(o1).isNotEqualTo(o2);
    }

    @Test
    public void useFieldByFieldComparing() {
        //Given
        SomeObject o1 = new SomeObject("name", 1);
        SomeObject o2 = new SomeObject("name", 1);
        //Expect
        assertThat(o1)
                .isEqualToComparingFieldByFieldRecursively(o2);
    }

    private static class SomeObject {
        private String someString;
        private int age;

        private SomeObject(String someString, int age) {
            this.someString = someString;
            this.age = age;
        }
    }
}
