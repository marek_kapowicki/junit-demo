package pl.marekk.junitdemo.domain.customer.api;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.Email;
import pl.marekk.junitdemo.api.CreateResourceRequest;
import pl.marekk.junitdemo.domain.customer.CustomerRegisterCommand;

import javax.validation.constraints.NotNull;

import static lombok.AccessLevel.PRIVATE;

@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor
@ToString
class CustomerRegisterRequest implements CreateResourceRequest {
    @NotNull String firstName;
    @NotNull String lastName;
    @NotNull @Email String email;

    CustomerRegisterCommand toCommand() {
        return CustomerRegisterCommand.of(firstName, lastName, email);
    }
}
