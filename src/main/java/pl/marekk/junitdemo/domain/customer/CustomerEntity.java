package pl.marekk.junitdemo.domain.customer;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import org.springframework.data.annotation.CreatedDate;
import pl.marekk.junitdemo.domain.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.StringJoiner;

@Entity
@Table(name = "customer")
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
final class CustomerEntity extends BaseEntity {
    @NotNull String firstName;
    @NotNull String lastName;
    @NotNull String email;

    @CreatedDate
    @NonFinal
    @Getter LocalDateTime creationTime;

    CustomerEntity(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    CustomerSnapshot toSnapshot() {
        return new CustomerSnapshot(getUuid(), fullName(), email);
    }

    private String fullName() {
        return new StringJoiner(" ")
                .add(firstName)
                .add(lastName).toString();
    }
}
