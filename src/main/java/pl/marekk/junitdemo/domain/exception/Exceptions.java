package pl.marekk.junitdemo.domain.exception;

import lombok.NoArgsConstructor;

import java.util.function.Supplier;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public final class Exceptions {
    public static Supplier<RuntimeException> notFound() {
        return ResourceNotFound::new;
    }

    public static Supplier<RuntimeException> notFound(String message) {
        return () -> new ResourceNotFound(message);
    }

    public static Supplier<RuntimeException> conflicted() {
        return ResourceConflicted::new;
    }

    public static Supplier<RuntimeException> conflicted(String message) {
        return () -> new ResourceConflicted(message);
    }
}
