package pl.marekk.junitdemo.api;

public interface Specification {
    String ROOT = "/api";
    String API_CONTENT_TYPE = "application/vnd.unit.v1+json";
}
